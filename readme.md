# Bienvenue sur le dépôt du module 14 Cup of tea

_Ce dépôt est la correction du module 14 en utilisant le CMS WordPress._

## Outils requis

* Apache2
* PHP
* MySQL

### Installation simple

* Télécharger ce projet.
* Dézipper l'archive dans le dossier où pointe votre serveur.
* Créer une base de données :
    * Nom de la bdd : `wp_3wa`
    * Nom de l'utilisateur : `wp_3wa`
    * Mot de passe de l'utilisateur : `wp_3wa`
* Aller sur `http://localhost/votre_dossier_wordpress/wp-admin/install.php`
