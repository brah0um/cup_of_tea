<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_3wa');

/** MySQL database username */
define('DB_USER', 'wp_3wa');

/** MySQL database password */
define('DB_PASSWORD', 'wp_3wa');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(?1bPZG^KGs$|!VcKG2tz P,:BcvKnpbJktfk]]:;Pd_#s7g5W_qaCg)kUkWi<E?');
define('SECURE_AUTH_KEY',  'FbRU(C3:W$7DXcP !F )b ypBAaA07E.Ev~M~nnga;2TJH=PKGTtr~r8gZc/%|!|');
define('LOGGED_IN_KEY',    'u TaLwbq()jcM#B710?9Lei|KA9M+79M4,pQU,y)0 $jJF{o;!% xO,7S;eS>=3%');
define('NONCE_KEY',        'G(dSU7A`rY!]{25s!7a.^ $=VK0pwb@KrBm-Sqd92?$~4` .NV1>M-?:{$?P:0lD');
define('AUTH_SALT',        '`DH/vx??n4MCZ]5bK_2WZEa;18%6bT-DIqK3<H)DI&/*[e()nXiRAe>/9nwZ_ZMM');
define('SECURE_AUTH_SALT', '?;H#!YMGl!SB@[%Wn`nZ]27=6 #n(M@ko=W}NUxRWTmLIKebV)kFPWD0+h/G$_hy');
define('LOGGED_IN_SALT',   'l`$NTUsN:~vHbwK5RO+L<><QN@Grw+iI>WLH~D.5XYm!~V6ZCCGjYu{r<iHs:fe2');
define('NONCE_SALT',       'P)=Nqyq_)Ag~wV.Pf%Kf<%ANwtAs/_!o%rWt+4=jO#8Z4[fP<^3p6=1oBMu3g86y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

