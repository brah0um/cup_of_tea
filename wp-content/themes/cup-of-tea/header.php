<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cup-of-tea
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <?php if ( is_front_page() && is_home() ) : ?>
            <link type="text/css" rel="stylesheet" href="<?php echo bloginfo('stylesheet_directory'); ?>/cup-of-tea.css" />
        <?php endif; ?>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>

        <header id="masthead" class="site-header" role="banner">
            <div class="topbar">
                <small>Livraison offerte à partir de 65€ d'achat !</small>
            </div>
            <div class="container">
                <a href="index.html"><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/logo.png" alt="logo de Cup of Tea" /></a>
                <div id="cart" class="right clear">
                    <p class="left">Mon panier <span>42,00€</span></p>
                    <i class="fa fa-shopping-cart right"></i>
                </div>
                <nav role="navigation">
                    <?php wp_nav_menu( array() ); ?>
                </nav><!-- #site-navigation -->
            </div>
        </header><!-- #masthead -->

        <div id="content" class="site-content">
