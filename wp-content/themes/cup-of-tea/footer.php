<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cup-of-tea
 */

?>

        <footer id="colophon" class="site-footer" role="contentinfo">
            <section id="info">
                <?php query_posts("post_type=information&order=ASC"); ?>

                <?php if ( have_posts() ) : ?>

                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) : the_post();

                        /*
                         * Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        include('template-parts/information/content-page.php');

                    endwhile;
                    ?>

                <?php endif; ?>
            </section>

            <!-- 
                Si tu lis ce commentaire je te donne un "petit exercice caché".
                Je te propose de changer les lignes de code en dessous en créant ton propre post_type
                dans le même principe que "informtion" au dessus.

                Good Luck & Have Fun !

                PS: La correction est présente quelque part dans le site. Si tu as besoin d'aide tu peux me contacter par mail : boukoufallah.brahim@gmail.com
             -->
            <section id="bottombar">
                <article>
                    <h2>Cup of Tea</h2>
                    <ul>
                        <li>Notre histoire</li>
                        <li>Nos boutiques</li>
                        <li>Le Thé de A à Z</li>
                        <li>Espace clients professionnels</li>
                        <li>Recrutement</li>
                        <li>Contactez-nous !</li>
                        <li>L'École du Thé</li>
                    </ul>
                </article>
                <article>
                    <h2>Commandez en ligne</h2>
                    <ul>
                        <li>Première visite</li>
                        <li>Aide - FAQ</li>
                        <li>Service client</li>
                        <li>Suivre ma commande</li>
                        <li>Conditions générales de vente</li>
                        <li>Informations légales</li>
                    </ul>
                </article>
                <article>
                    <h2>Suivez-nous !</h2>
                    <ul>
                        <li>Notre histoire</li>
                        <li>Nos boutiques</li>
                        <li>Le Thé de A à Z</li>
                        <li>Espace clients professionnels</li>
                    </ul>
                </article>
            </section>

        <?php wp_footer(); ?>

    </body>
</html>
