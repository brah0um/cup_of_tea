<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cup-of-tea
 */
get_header(); ?>

    <main class="container" role="main">
        <section id="slider">
            <h2>Le thé, les bienfaits<br /><span>Lorem ipsum dolor sit amet consectetur adipisicing</span></h2>
            <img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/the.jpg" alt="" />
        </section>

        <section id="select">
            <h1>Choisissez votre thé</h1>

            <!--
                On prépare un filtre à la requête.

                post_type : On récupère les articles de type tea.
                (Vas voir la fonction create_tea_type() dans functions.php).

                order : On trie les articles dans l'ordre croissant.
            -->
            <?php query_posts("post_type=tea&order=ASC"); ?>

            <?php if ( have_posts() ) : ?>
                
                <?php
                /*
                 *  On commence une boucle en récupérant tous les postes relatifs à notre filtre query_posts.
                 *  On récupère donc tous les articles (= posts) de type "tea".
                 */
                while ( have_posts() ) : the_post();

                    /*
                     * À chaque boucle on inclut le code html présent dans template-parts/tea
                     * Ce code sera inclut à chaque article.
                     *
                     * Lorsque vous aurez envie de faire votre propre post_type, il faudra alors
                     * créer un "template" (modèle de page) pour un article.
                     */
                    include('template-parts/tea/content-page.php');

                endwhile;
                ?>

            <?php endif; ?>
        </section>

        <section id="news">
            <?php query_posts("post_type=post&order=ASC"); ?>
            <?php if ( have_posts() ) : ?>

                <?php
                while ( have_posts() ) : the_post();
                    include('template-parts/news/content-page.php');
                endwhile;
                ?>

            <?php endif; ?>
        </section>
    </main><!-- #main -->

<?php
get_footer();
