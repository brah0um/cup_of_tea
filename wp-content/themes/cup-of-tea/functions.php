<?php
/**
 * cup-of-tea functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package cup-of-tea
 */

if ( ! function_exists( 'cup_of_tea_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function cup_of_tea_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on cup-of-tea, use a find and replace
     * to change 'cup-of-tea' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'cup-of-tea', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary', 'cup-of-tea' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    // Set up the WordPress core custom background feature.
    add_theme_support( 'custom-background', apply_filters( 'cup_of_tea_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
    ) ) );
}
endif;
add_action( 'after_setup_theme', 'cup_of_tea_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function cup_of_tea_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'cup_of_tea_content_width', 640 );
}
add_action( 'after_setup_theme', 'cup_of_tea_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cup_of_tea_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'cup-of-tea' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'cup-of-tea' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'cup_of_tea_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function cup_of_tea_scripts() {
    wp_enqueue_style( 'cup-of-tea-style', get_stylesheet_uri() );

    wp_enqueue_script( 'cup-of-tea-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

    wp_enqueue_script( 'cup-of-tea-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'cup_of_tea_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/************************************
 *** Taxonomies related functions ***
 ************************************/

/**
 * Set and return the labels for a new post type
 *
 * @param $singular_type : the plural of the label
 * @param $plural_type : the plural of the label
 * @return array
 */
function getLabels($singular_type, $plural_type)
{
    return array(
        'name'               => __($singular_type, 'post type general name', 'cup-of-tea'),
        'singular_name'      => __($singular_type, 'post type singular name', 'cup-of-tea'),
        'add_new'            => __('Add New', 'cup-of-tea'),
        'add_new_item'       => __(sprintf('Add new %s', $singular_type), 'cup-of-tea'),
        'edit_item'          => __(sprintf('Edit %s', $singular_type), 'cup-of-tea'),
        'new_item'           => __(sprintf('New %s', $singular_type), 'cup-of-tea'),
        'all_items'          => __(sprintf('All %s', $plural_type), 'cup-of-tea'),
        'view_item'          => __(sprintf('View %s', $singular_type), 'cup-of-tea'),
        'search_items'       => __(sprintf('Search %s', $plural_type), 'cup-of-tea'),
        'not_found'          =>  __(sprintf('No %s found', $plural_type), 'cup-of-tea'),
        'not_found_in_trash' => __(sprintf('No %s found in trash', $plural_type), 'cup-of-tea'),
        'parent_item_colon'  => '',
        'menu_name'          => __(sprintf('%s', $plural_type), 'cup-of-tea')
    );
}

/**
 * Create the tea type
 */
function create_tea_type()
{
    // On appelle la fonction getLabels que nous avons déclarée au dessus.
    $labels = getLabels('Tea', 'Teas');
    $args = array(
        'labels'       => $labels,
        'public'       => true,
        'has_archive'  => true,
        'hierarchical' => true,
        'supports'     =>  array('title', 'page-attributes', 'editor')
    );

    // On enregistre le nouveau type d'articles. Le post_type a pour identifiant "tea"
    register_post_type('tea', $args);
}

/**
 * Create the information type
 */
function create_information_type()
{
    $labels = getLabels('Information', 'Informations');
    $args = array(
        'labels'       => $labels,
        'public'       => true,
        'has_archive'  => true,
        'hierarchical' => true,
        'supports'     =>  array('title', 'page-attributes', 'editor')
    );

    // On enregistre le nouveau type d'articles. Le post_type a pour identifiant "tea"
    register_post_type('information', $args);
}

// Hook qui va lancer la fonction create_tea_type à l'initialisation de WordPress
add_action('init', 'create_tea_type');
add_action('init', 'create_information_type');
