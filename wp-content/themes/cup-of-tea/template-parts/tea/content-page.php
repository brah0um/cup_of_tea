<!-- Template des thés -->
<article  id="tea-<?php the_ID(); ?>" class="tea">
    <p><?php the_content(); ?></p>
    <?php the_title( '<h3>', '</h3>' ); ?>
</article>
