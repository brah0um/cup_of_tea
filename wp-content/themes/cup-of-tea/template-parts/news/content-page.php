<!-- Teplate nos nouveautés | nos best-sellers | notre coup de coeur -->
<!-- La fonction the_field() permet d'afficher nos champs custom via leur nom de champ défini dans le BackOffice -->
<article id="<?php the_field('element_id'); ?>">
    <?php the_title( '<h2>', '</h2>' ); ?>
    <p><?php the_content(); ?></p>
    <h3><?php the_field('sub_title'); ?></h3>
    <small><?php the_field('sub_sub_title'); ?></small>
</article>
