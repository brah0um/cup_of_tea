<?php
// Les fonctions suivantes sont à mettre dans functions.php

/**
 * Create the about type
 */
function create_about_type()
{
    $labels = getLabels('About', 'Abouts');
    $args = array(
        'labels'       => $labels,
        'public'       => true,
        'has_archive'  => true,
        'hierarchical' => true,
        'supports'     =>  array('title', 'page-attributes', 'editor')
    );

    // On enregistre le nouveau type d'articles. Le post_type a pour identifiant "about"
    register_about_type('about', $args);
}

// Hook qui va lancer la fonction create_tea_type à l'initialisation de WordPress
add_action('init', 'create_about_type');
?>

<!-- Ceci est le bout de code à remplacer dans le fichier footer.php -->
<section id="bottombar">
    <?php query_posts("post_type=about&order=ASC"); ?>

    <?php if ( have_posts() ) : ?>

        <?php while ( have_posts() ) : the_post(); ?>

            <article>
                <h2><?php the_title(); ?></h2>

                <!--
                    J'utilise les tags pour afficher les éléments de ma liste.
                    Je ne pense pas que ce soit la meilleur solution.

                    À toi d'en trouver une mieux et n'hésites pas à m'en faire part ;)
                 -->
                <?php the_tags( '<ul><li>', '</li><li>', '</li></ul>' ); ?>
            </article>

        <?php endwhile; ?>

    <?php endif; ?>
</section>
